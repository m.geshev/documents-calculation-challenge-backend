<?php

use App\Exceptions\DataException;
use App\Models\Currency;
use App\Models\Report;
use App\Services\DataService;

class DataServiceTest extends TestCase
{
    private $dataService;

    public function setUp(): void
    {
        $this->dataService = new DataService();
        parent::setUp();
    }

    /**
     * Test processing valid data.
     *
     * @return void
     */
    public function testProcessingValidData()
    {
        $filePath = __DIR__ . '/data/data.csv';

        $this->dataService->setData($filePath);
        $this->dataService->setCurrencies([
            new Currency('EUR', 1),
            new Currency('USD', 0.897),
            new Currency('GBP', 1.19)
        ]);

        $reports = $this->dataService->getResults('EUR');
        $this->assertContainsOnlyInstancesOf(Report::class, $reports);
        $this->assertEquals('101.19', $reports[2]->getTotal());
        $this->assertEquals('EUR', $reports[2]->getCurrency());

        $reports = $this->dataService->getResults('USD');
        $this->assertEquals('355.96', $reports[0]->getTotal());
        $this->assertEquals('USD', $reports[0]->getCurrency());


        $reports = $this->dataService->getResults('GBP');
        $this->assertEquals('605.55', $reports[1]->getTotal());
        $this->assertEquals('GBP', $reports[1]->getCurrency());
    }

    /**
     * Test filtering data by vat number.
     *
     * @return void
     */
    public function testFilteringDataByVatNumber()
    {
        $filePath = __DIR__ . '/data/data.csv';

        $this->dataService->setData($filePath);
        $this->dataService->setCurrencies([
            new Currency('EUR', 1),
            new Currency('USD', 0.897),
            new Currency('GBP', 1.19)
        ]);

        $reports = $this->dataService->getResults('EUR', '123465123');

        $this->assertContainsOnlyInstancesOf(Report::class, $reports);
        $this->assertCount(1, $reports);
        $this->assertEquals('123465123', $reports[0]->getVat());
    }

    /**
     * Test processing invalid data structure.
     *
     * @return void
     */
    public function testProcessingInvalidDataStructure()
    {
        $filePath = __DIR__ . '/data/invalid_data_structure.csv';

        $this->expectException(DataException::class);
        $this->expectExceptionMessage('Missing some of the required fields: vat_number,document_number');
        $this->dataService->setData($filePath);
    }

    /**
     * Test processing invalid data documents.
     *
     * @return void
     */
    public function testProcessingInvalidDataReports()
    {
        $filePath = __DIR__ . '/data/invalid_data_documents.csv';

        $this->expectException(DataException::class);
        $this->expectExceptionMessage('Missing some of the following document numbers: 100000025777');
        $this->dataService->setData($filePath);
    }

    /**
     * Test undefined currency.
     *
     * @return void
     */
    public function testUndefinedCurrency()
    {
        $filePath = __DIR__ . '/data/data.csv';

        $this->dataService->setData($filePath);
        $this->dataService->setCurrencies([
            new Currency('EUR', 1),
            new Currency('USD', 0.897)
        ]);

        $this->expectException(DataException::class);
        $this->expectExceptionMessage('GBP currency undefined.');
        $reports = $this->dataService->getResults('EUR');
    }

    /**
     * Test missing output currency.
     *
     * @return void
     */
    public function testMissingOutputCurrency()
    {
        $filePath = __DIR__ . '/data/data.csv';

        $this->dataService->setData($filePath);
        $this->dataService->setCurrencies([
            new Currency('EUR', 1),
            new Currency('USD', 0.897),
            new Currency('GBP', 1.19)
        ]);

        $this->expectException(DataException::class);
        $this->expectExceptionMessage('XXX currency undefined.');
        $reports = $this->dataService->getResults('XXX');
    }
}
