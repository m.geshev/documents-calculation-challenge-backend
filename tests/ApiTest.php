<?php

use Illuminate\Http\UploadedFile;

class ApiTest extends TestCase
{
    /**
     * Test json response structure.
     *
     * @return void
     */
    public function testJsonResponseStructure()
    {
        $path = __DIR__ . '/data/data.csv';
        $file = new UploadedFile($path, 'data.csv', 'text/csv', null, true);
        $data = [
            'currency' => 'EUR',
            'exchange_rates' => [
                ['currency' => 'EUR', 'rate' => 1],
                ['currency' => 'USD', 'rate' => 0.89],
                ['currency' => 'GBP', 'rate' => 1.19]
            ]
        ];

        $this->call('POST', '/', $data, [], ['data' => $file]);

        $this->assertResponseStatus(200);
        $this->seeJsonStructure([
            "*" => [
                "vat",
                "customer",
                "currency",
                "total",
            ]
        ]);
    }

    /**
     * Test filtering results by vat number.
     *
     * @return void
     */
    public function testFilteringResultsByVatNumber()
    {
        $path = __DIR__ . '/data/data.csv';
        $file = new UploadedFile($path, 'data.csv', 'text/csv', null, true);
        $data = [
            'currency' => 'GBP',
            'exchange_rates' => [
                ['currency' => 'EUR', 'rate' => 1],
                ['currency' => 'USD', 'rate' => 0.89],
                ['currency' => 'GBP', 'rate' => 1.19]
            ],
            'vat' => '987654321'
        ];

        $response = $this->call('POST', '/', $data, [], ['data' => $file]);
        $result = json_decode($response->content(), true);

        $this->assertResponseStatus(200);
        $this->assertCount(1, $result);
        $this->assertEquals('987654321', $result[0]['vat']);
        $this->assertEquals('GBP', $result[0]['currency']);
        $this->assertEquals('Vendor 2', $result[0]['customer']);
    }

    /**
     * Test processing data with undefined currency.
     *
     * @return void
     */
    public function testDataWithUndefinedCurrency()
    {
        $path = __DIR__ . '/data/data.csv';
        $file = new UploadedFile($path, 'data.csv', 'text/csv', null, true);
        $data = [
            'currency' => 'USD',
            'exchange_rates' => [
                ['currency' => 'EUR', 'rate' => 1],
                ['currency' => 'USD', 'rate' => 0.89]
            ]
        ];

        $this->call('POST', '/', $data, [], ['data' => $file]);

        $this->assertResponseStatus(500);
        $this->seeJson([
            'Currency' => ['GBP currency undefined.']
        ]);
    }
}
