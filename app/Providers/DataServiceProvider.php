<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\DataService;

class DataServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataService::class, function(){
            return new DataService;
        });
    }

}
