<?php

namespace App\Services;

use App\Exceptions\DataException;
use App\Models\Currency;
use App\Models\Document;
use App\Models\Report;

class DataService
{
    const REQUIRED_FIELDS = ['customer', 'vat_number', 'document_number', 'type', 'parent_document', 'currency', 'total'];
    const TYPES = [1 => 'invoice', 2 => 'credit_note', 3 => 'debit_note'];

    private array $data = [];
    private array $currencies = [];

    /**
     * Read and validate data from input file
     * 
     * @param string $fileName
     * 
     * @throws \Exception
     */
    public function setData($fileName): void
    {
        $data = [];
        if (($fopen = fopen($fileName, "r")) !== FALSE) {
            $fields = false;
            while (($result = fgetcsv($fopen, 1000, ",")) !== FALSE) {
                if ($fields) {
                    $i = 0;
                    $row = [];
                    foreach ($result as $val) {
                        $key = isset($fields[$i]) ? $fields[$i] : 'unknown';
                        $row[$key] = $val;
                        $i++;
                    }
                    $data[] = new Document($row);
                } else {
                    $fields = [];
                    foreach ($result as $val) {
                        $fields[] = str_replace(' ', '_', strtolower($val));
                    }
                    $this->validateFields($fields);
                }
            }

            fclose($fopen);
        }

        $this->validateData($data);

        $this->data = $data;
    }

    /**
     * Get currency by name
     * 
     * @param string $name
     * 
     * @return Currency $currency
     * @throws \Exception
     */
    public function getCurrency(string $name): Currency
    {
        foreach ($this->currencies as $currency) {
            if ($currency->getName() == strtoupper($name)) {
                return $currency;
            }
        }

        throw new DataException('Currency', $name . ' currency undefined.');
    }

    /**
     * Set currencies
     * 
     * @param Currency[] $currencies
     * 
     * @throws \Exception
     */
    public function setCurrencies(array $currencies): void
    {
        foreach ($currencies as $currency) {
            if (!$currency instanceof Currency) {
                throw new DataException('Currency', 'Currency instances supported only.');
            }
        }

        $this->currencies = $currencies;
    }

    /**
     * Returns calculations of all totals in the selected currency filtered by vat number
     * 
     * @param string $currencyName
     * @param string $vatNumber
     * 
     * @return Report[] $reports
     * @throws \Exception
     */
    public function getResults(string $currencyName, string $vatNumber = null): array
    {
        $targetCurrency = $this->getCurrency($currencyName);
        $totals = [];
        foreach ($this->data as $doc) {
            if (!isset($totals[$doc->getVatNumber()])) {
                $totals[$doc->getVatNumber()] = ['vat' => $doc->getVatNumber(), 'customer' => $doc->getCustomer(), 'currency' => $targetCurrency->getName(), 'total' => 0];
            }

            $currency = $this->getCurrency($doc->getCurrency());

            $subTotal = (float)$doc->getTotal();
            $subTotal = $subTotal * $currency->getExchangeRate(); // Multiply sub total by currency exchange rate to convert it to base currency.
            $subTotal = $subTotal * (float)(($doc->getType() === 'credit_note') ? -1 : 1); // If type is 2 (credit_note) then multiply by (-1) to subsctract.

            $totals[$doc->getVatNumber()]['total'] += $subTotal * (float)(1 / $targetCurrency->getExchangeRate()); // Convert current sub total to target currency and add it to total.
        }

        $results = array_values($totals);

        if ($vatNumber) {
            $results = isset($totals[$vatNumber]) ? [$totals[$vatNumber]] : [];
        }

        $reports = [];
        foreach ($results as $result) {
            $reports[] = new Report(
                $result['vat'],
                $result['customer'],
                $result['currency'],
                $result['total']
            );
        }

        return $reports;
    }

    /**
     * Validates if all required fields exists
     * 
     * @param array $fields
     * 
     * @throws \Exception
     */
    private function validateFields(array $fields): void
    {
        $requiredFields = Document::REQUIRED_PROPERTIES;
        if ($diff = array_diff($requiredFields, $fields)) {
            throw new DataException('Invalid data', 'Missing some of the required fields: ' . implode(',', $diff));
        }
    }

    /**
     * Validates data document relations
     * 
     * @param Documents $data
     * 
     * @throws \Exception
     */
    private function validateData(array $documents): void
    {
        $docNumbers = [];
        $parDocNumbers = [];

        foreach ($documents as $document) {
            if (!empty($document->getDocumentNumber())) {
                $docNumbers[] = $document->getDocumentNumber();
            }

            if (!empty($document->getParentDocumentNumber())) {
                $parDocNumbers[] = $document->getParentDocumentNumber();
            }
        }

        if ($diff = array_diff($parDocNumbers, $docNumbers)) {
            throw new DataException('Invalid data', 'Missing some of the following document numbers: ' . implode(',', $diff));
        }
    }
}
