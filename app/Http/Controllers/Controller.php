<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\DataService;
use App\Helpers\DataHelper;
use App\Exceptions\DataException;

class Controller extends BaseController
{
    protected $dataService;

    public function __construct(DataService $dataService)
    {
        $this->dataService = $dataService;
    }

    /**
     * Retrieve results by the given data.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'data' => 'required|mimes:csv,txt',
            'exchange_rates' => 'required|array',
            'exchange_rates.*.currency' => 'required|string|min:3|max:3',
            'exchange_rates.*.rate' => 'required|regex:/^\d+(\.\d{1,6})?$/', // Float format 0.00
            'currency' => 'required|string|min:3|max:3',
            'vat' => ''
        ]);

        try {
            $dataFile = $request->file('data');
            $exchangeRates = $request->post('exchange_rates');
            $currency = $request->post('currency');
            $vatNumber = $request->post('vat');

            $currencies = [];
            foreach ($exchangeRates as $exchangeRate) {
                $currencies[] = new Currency($exchangeRate['currency'], (float)$exchangeRate['rate']);
            }

            $this->dataService->setData($dataFile);
            $this->dataService->setCurrencies($currencies);

            $documents = $this->dataService->getResults($currency, $vatNumber);

            $json = [];
            foreach ($documents as $document) {
                $json[] = $document->toArray();
            }

            return response()->json($json);
        } catch (DataException $e) {
            return response()->json([
                $e->getTitle() => [$e->getMessage()]
            ], 500);
        }
    }
}
