<?php

namespace App\Models;

class Currency
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $exchangeRate;

    /**
     * Currency constructor
     *
     * @param  string  $name
     * @param  float  $exchangeRate
     */
    public function __construct(string $name, float $exchangeRate)
    {
        $this->setName($name);
        $this->setExchangeRate($exchangeRate);
    }

    /**
     * Get currency name
     * 
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set currency name
     * 
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = strtoupper($name);
    }

    /**
     * Get currency exchange rate
     * 
     * @return float
     */
    public function getExchangeRate(): float
    {
        return $this->exchangeRate;
    }

    /**
     * Set currency exchange rate
     * 
     * @param float $exchangeRate
     */
    public function setExchangeRate(float $exchangeRate): void
    {
        $this->exchangeRate = $exchangeRate;
    }
}
