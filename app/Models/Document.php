<?php

namespace App\Models;

class Document
{
    const REQUIRED_PROPERTIES = ['customer', 'vat_number', 'document_number', 'type', 'parent_document', 'currency', 'total'];
    const TYPES = [1 => 'invoice', 2 => 'credit_note', 3 => 'debit_note'];

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $vatNumber;

    /**
     * @var string
     */
    private $customer;

    /**
     * @var string
     */
    private $documentNumber;

    /**
     * @var string|null
     */
    private $parentDocumentNumber;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var float
     */
    private $total;

    /**
     * Document constructor
     *
     * @param  array|null  $data
     */
    public function __construct(array $data = null)
    {
        if ($data) {
            $this->fromArray($data);
        }
    }

    /**
     * Constructs the object with the given data
     *
     * @param array $data
     * 
     * @return Document
     */
    public function fromArray(array $data): Document
    {
        if (isset($data['type'])) {
            $this->setType($data['type']);
        }
        if (isset($data['vat_number'])) {
            $this->setVatNumber($data['vat_number']);
        }
        if (isset($data['customer'])) {
            $this->setCustomer($data['customer']);
        }
        if (isset($data['document_number'])) {
            $this->setDocumentNumber($data['document_number']);
        }
        if (isset($data['parent_document'])) {
            $this->setParentDocumentNumber($data['parent_document']);
        }
        if (isset($data['currency'])) {
            $this->setCurrency($data['currency']);
        }
        if (isset($data['total'])) {
            $this->setTotal($data['total']);
        }

        return $this;
    }

    /**
     * Get type
     * 
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set type
     * 
     * @param int $type
     */
    public function setType(int $type): void
    {
        $types = self::TYPES;
        if (!isset($types[$type])) {
            throw new \Exception('Unsupported document type.');
        }

        $this->type = $types[$type];
    }

    /**
     * Get vat number
     * 
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * Set vat number
     * 
     * @param string $vatNumber
     */
    public function setVatNumber(string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * Get customer
     * 
     * @return string
     */
    public function getCustomer(): string
    {
        return $this->customer;
    }

    /**
     * Set customer
     * 
     * @param string $customer
     */
    public function setCustomer(string $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * Get document number
     * 
     * @return string
     */
    public function getDocumentNumber(): string
    {
        return $this->documentNumber;
    }

    /**
     * Set document number
     * 
     * @param string $documentNumber
     */
    public function setDocumentNumber(string $documentNumber): void
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * Get parent document number
     * 
     * @return string|null
     */
    public function getParentDocumentNumber(): ?string
    {
        return $this->parentDocumentNumber;
    }

    /**
     * Set parent document number
     * 
     * @param string|null $parentDocumentNumber
     */
    public function setParentDocumentNumber(string $parentDocumentNumber = null): void
    {
        $this->parentDocumentNumber = $parentDocumentNumber;
    }

    /**
     * Get currency
     * 
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * Set currency
     * 
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * Get total
     * 
     * @return string
     */
    public function getTotal(): string
    {
        return number_format($this->total, 2);
    }

    /**
     * Set total
     * 
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }
}
