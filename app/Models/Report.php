<?php

namespace App\Models;

class Report
{
    /**
     * @var string
     */
    private $vat;

    /**
     * @var string
     */
    private $customer;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var float
     */
    private $total;

    /**
     * Report constructor
     *
     * @param  string  $vat
     * @param  string  $customer
     * @param  string  $currency
     * @param  float  $total
     */
    public function __construct(
        string $vat,
        string $customer,
        string $currency,
        float $total
    ) {
        $this->setVat($vat);
        $this->setCustomer($customer);
        $this->setCurrency($currency);
        $this->setTotal($total);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'vat' => $this->getVat(),
            'customer' => $this->getCustomer(),
            'currency' => $this->getCurrency(),
            'total' => $this->getTotal()
        ];
    }

    /**
     * Get vat
     * 
     * @return string
     */
    public function getVat(): string
    {
        return $this->vat;
    }

    /**
     * Set vat
     * 
     * @param string $vat
     */
    public function setVat(string $vat): void
    {
        $this->vat = $vat;
    }

    /**
     * Get customer
     * 
     * @return string
     */
    public function getCustomer(): string
    {
        return $this->customer;
    }

    /**
     * Set customer
     * 
     * @param string $customer
     */
    public function setCustomer(string $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * Get currency
     * 
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * Set currency
     * 
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * Get total
     * 
     * @return string
     */
    public function getTotal(): string
    {
        return number_format($this->total, 2);
    }

    /**
     * Set total
     * 
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }
}
