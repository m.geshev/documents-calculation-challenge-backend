# Documents Calculation Challenge Backend



## Installing

```
#clone the project from the repository
git clone https://gitlab.com/m.geshev/documents-calculation-challenge-backend.git

#install the project from the root folder of the project
Run `composer install`
```

## Serve the application
```
#serve your project locally
Run `php -S localhost:8000 -t public` from the root folder of the project.
```

## Running Tests

```
#for unit tests
Run `vendor/bin/phpunit` from the root folder of the project.
```
